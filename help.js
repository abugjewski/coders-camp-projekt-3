const region = document.getElementById("region-select");
const type = document.getElementById("type-select");
const rarity = document.getElementById("rarity-select");
const minCost = document.getElementById("min-cost");
const maxCost = document.getElementById("max-cost");

const deckCardsAmount = document.querySelector(".deck-cards__amount");
const followersAmount = document.querySelectorAll(".deck-cards__type")[0].querySelector(".type__amount");
const followersList = document.querySelectorAll(".deck-cards__type")[0].querySelector(".type__list");
const spellsAmount = document.querySelectorAll(".deck-cards__type")[1].querySelector(".type__amount");
const spellsList = document.querySelectorAll(".deck-cards__type")[1].querySelector(".type__list");
const championsAmount = document.querySelectorAll(".deck-cards__type")[2].querySelector(".type__amount");
const championsList = document.querySelectorAll(".deck-cards__type")[2].querySelector(".type__list");

const cardsList = document.querySelector(".cards-list");